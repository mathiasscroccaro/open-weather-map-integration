#!/bin/sh

BASE_FOLDER=~/open-weather-map-integration

cd $BASE_FOLDER

container_id=$(docker ps -a -q --filter="name=weather_api")
[ ! -z $container_id ] && docker stop $container_id && docker rm $container_id

image_id=$(docker images --format '{{.Repository}}:{{.Tag}}' | grep 'weather_api')
[ ! -z $image_id ] && docker rmi $image_id

docker-compose build
docker-compose up -d

